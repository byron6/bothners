<?php
class WC_Product_Stm_Zoom extends WC_Product_Simple {

    public function __construct($product) {
        $this->product_type = 'stm_zoom';

        parent::__construct( $product );
    }

    /**
     * Return the product type
     * @return string
     */
    public function get_type() {
        return 'stm_zoom';
    }

}