(function ($) {
    'use strict';
    $(document).ready(function () {
        $('.stm_zoom_addons .addon_action a').on('click', function (e) {
            e.preventDefault();
            var addon = $(this).data('addon');
            var status = $(this).data('status');
            var wrap = $(this).closest('.addon_item');
            $.ajax({
                method: 'post',
                type: 'json',
                url: stm_wpcfto_ajaxurl,
                data: {
                    action: 'stm_zoom_update_addon',
                    addon: addon,
                    status: status,
                },
                success(r) {
                    wrap.removeClass('enable disable');
                    wrap.addClass(status)
                }
            });
        })
    });
})(jQuery);