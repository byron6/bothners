<?php
/**
 * Plugin Name: WooCommerce Inventory Tracking
 * Description: This plugin adds the ability to track inventory independently across multiple locations on identical products/items.

 * Version: 3.4.6

 * WC requires at least: 3.0
 * WC tested up to: 3.4.6

 * Author: Plugin Territory
 * Author URI: http://pluginterritory.com

 * Copyright: 2015-18 Plugin Territory
 * License: GNU General Public License v2.0 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
	
**/


// i18n
function pt_wcit_action_init() {
	load_plugin_textdomain( 'wcit-pluginterritory', null, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'pt_wcit_action_init' );

// easy settings access
function pt_wcit_action_links( $links, $file ) {
	if ( 'wc-inventory-tracking-pluginterritory/plugin.php' == $file ) {
		$page_link = '<a href="' .  admin_url( 'admin.php?page=wc-settings&tab=products&section=inventory-locations' ) . '">' . __( 'Inventory Locations', 'wcit-pluginterritory' ) . '</a>';
		// make page_link appear first
		array_unshift( $links, $page_link );
	}
	return $links;
}
add_filter( 'plugin_action_links', 'pt_wcit_action_links', 10, 2 );

// Create our section beneath the products tab
function pt_wcit_add_section( $sections ) {
	
	$new_sections = array();
	foreach( $sections as $key => $value ) {
		if ( 'downloadable' == $key ) {
			$new_sections['inventory-locations'] = __( 'Inventory Locations', 'wcit-pluginterritory' );
		}
		$new_sections[ $key ] = $value;
	}
	return $new_sections;
	
}
add_filter( 'woocommerce_get_sections_products', 'pt_wcit_add_section' );


// Disable adding to cart at the store loop as one must select location
function pt_wcit_add_to_cart_link( $link, $product) {
	return 	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button product_type_%s">%s</a>',
		esc_url( get_permalink( $product->id ) ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		esc_attr( $product->product_type ),
		esc_html(  __( 'Select options', 'woocommerce' ) )
	);
}
add_filter( 'woocommerce_loop_add_to_cart_link', 'pt_wcit_add_to_cart_link', 10, 2 );

function pt_wcit_set_html_content_type() {
	return 'text/html';
}

//
add_action( 'admin_init', 'pt_wcit_admin_init' );
function pt_wcit_admin_init() {

	add_action( 'woocommerce_product_options_stock_fields',  'pt_wcit_add_stock_settings' );
	add_filter( 'woocommerce_inventory_settings',            'pt_wcit_inventory_settings' );
	add_filter( 'woocommerce_get_settings_products',         'pt_wcit_extra_inventory_settings', 10, 2 );


	add_action( 'woocommerce_process_product_meta',          'pt_wcit_process_product_meta' );

	add_action( 'woocommerce_product_after_variable_attributes',  'pt_wcit_add_variation_settings', 10, 3 );
	add_action( 'woocommerce_process_product_meta_variable',      'pt_wcit_process_variation_settings' ); 
	add_action( 'woocommerce_update_product_variation',           'pt_wcit_process_variation_settings' ); // account for AJAX updates

	add_action( 'woocommerce_product_options_stock', 'pt_wcit_show_message' );
	add_action( 'woocommerce_product_after_variable_attributes',  'pt_wcit_show_message', 5 );

}

function pt_wcit_inventory_settings( $settings ) {
	$settings[] = array(
						'title' => __( 'Number of inventory locations', 'wcit-pluginterritory' ),
						'type' => 'title',
						'id' => 'wcit_inventory_options',
						'desc' 	=>  __( 'Set the number of the inventory locations.', 'wcit-pluginterritory' ),
						);


	$settings[] = array(
							'title'         => __( 'How many locations exists', 'wcit-pluginterritory' ), 
							'desc'              => __( 'You need to have at least one location.', 'wcit-pluginterritory' ),
							'id'            => 'pt_wcit_number_of_inventory_locations',
							'css'               => 'width:50px;',
							'type'              => 'number',
							'custom_attributes' => array(
								'min'  => 1,
								'step' => 1,
							),
						'default'           => '5',
						'desc_tip'          => true,
						);


	$settings[] = array(
						'type' => 'sectionend',
						'id' => 'wcit_inventory_options'
					);
	return $settings;
}


// shows a message about automatic stock calculation
function pt_wcit_show_message() {
	printf( '<p>%s</p>', __( "The stock quantity will be automatically calculated by the sum of the locations' stocks.", 'wcit-pluginterritory') );
}

// update custom stocks for variations
function pt_wcit_process_variation_settings() {

	if ( ( 'variable' == $_POST[ 'product-type' ] ) && isset( $_POST['variable_post_id'] ) && is_array( $_POST['variable_post_id'] ) ) {
        foreach( $_POST['variable_post_id'] as $k => $id ) {
			$i = $stock = 0;
			while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

				$i++;
				$stock +=  wc_clean( $_POST[ '_variable_stock_' . $i ][ $k ] );
				update_post_meta( $id, '_variable_stock_' . $i, wc_clean( $_POST[ '_variable_stock_' . $i ][ $k ] ) );

			}
			// update variable product stock
			update_post_meta( $id, '_stock', $stock );
        }
    }
}

// display custom stocks fields for variations
function pt_wcit_add_variation_settings( $loop, $variation_data, $variation ) {

	if ( 'yes' != get_option( 'woocommerce_manage_stock' ) )
		return;

	echo '<div class="show_if_variation_manage_stock" style="display: none;">';
	$i = 0;
	while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

		$i++;
		$location_id      = $i;
		$location_name    = get_option('pt_wcit_location_' . $i );
		$location_enabled = get_option('pt_wcit_location_enabled_' . $i );
		if ( 'yes' != $location_enabled ) {
			continue;
		}

		$value            = get_post_meta( $variation->ID, '_variable_stock_' . $location_id, true );

		// Location Stock
		woocommerce_wp_text_input( array(
			'id'                => '_variable_stock_' . $location_id . '[' . $loop . ']' ,
			'label'             => sprintf( __( 'Stock Qty for<br />%s',  'wcit-pluginterritory' ),  $location_name ),
			'value'             => $value,
			'desc_tip'          => true,
			'description'       => __( 'Enter a quantity to enable stock management at variation level, or leave blank to use the parent product\'s options.', 'woocommerce' ),
			'type'              => 'number',
			'custom_attributes' => array(
				'step' => 'any'
			),
			'data_type'         => 'stock'
		) );
	}
	echo '</div>';
}

// display locations fields
function pt_wcit_extra_inventory_settings( $settings,$current_section ) {
	/**
	 * Check the current section is what we want
	 **/
	if ( $current_section == 'inventory-locations' ) {

		$settings = array();
		if ( ! empty( get_option( 'pt_wcit_number_of_inventory_locations' ) ) ) {
			$desc = __( 'Set the names of the inventory locations.', 'wcit-pluginterritory' );	
		} else {
			$desc = sprintf( __( 'You need to first set the number of inventory locations at <a href="%s">Inventory</a>.', 'wcit-pluginterritory' ),  
				admin_url( 'admin.php?page=wc-settings&tab=products&section=inventory' ) );
		}
		
		$settings[] = array(
							'title' => sprintf( __( 'Inventory locations', 'wcit-pluginterritory' ) ),
							'type' => 'title',
							'id' => 'wcit_extra_inventory_options',
							'desc' 	=>  $desc,
							);

		$i = 0;
		while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )) {

			$i++;
			$settings[] = array(
									'title'         => sprintf( __( '%s location name', 'wcit-pluginterritory' ), date( 'jS', strtotime( '1980-01-' . $i ) ) ),
									'id'            => 'pt_wcit_location_' . $i,
									'type'     => 'text',
									'css'      => 'min-width:300px;',
								);

			$settings[] = array(
									'title'         => sprintf( __( 'Enable %s location name', 'wcit-pluginterritory' ), date( 'jS', strtotime( '1980-01-' . $i ) ) ),
									'id'            => 'pt_wcit_location_enabled_' . $i,
									'type'     => 'checkbox',
								);
		}

		$settings[] = array(
							'type' => 'sectionend',
							'id' => 'wcit_extra_inventory_options'
						);
	}
	return $settings;
}

// stock fields for simple products
function pt_wcit_add_stock_settings() {
	$i = 0;
	while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

		$i++;
		$location_id   = $i;
		$location_name = get_option('pt_wcit_location_' . $i );
		$location_enabled = get_option('pt_wcit_location_enabled_' . $i );
		if ( 'yes' != $location_enabled ) {
			continue;
		}

		// Location Stock
		woocommerce_wp_text_input( array(
			'id'                => '_stock_' . $location_id,
			'label'             => sprintf( __( 'Stock Qty for<br />%s',  'wcit-pluginterritory' ),  $location_name ),
			'desc_tip'          => true,
			'description'       => __( 'Stock quantity. If this is a variable product this value will be used to control stock for all variations, unless you define stock at variation level.', 'woocommerce' ),
			'type'              => 'number',
			'custom_attributes' => array(
				'step' => 'any'
			),
			'data_type'         => 'stock'
		) );
	}
}
// update custom stock for simple products
function pt_wcit_process_product_meta( $post_id ) {

	if ( 'simple' == $_POST[ 'product-type'] ) {

		$i = $stock = 0;
		while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

			$i++;
			$stock +=  wc_clean( $_POST[ '_stock_' . $i ] );
			update_post_meta( $post_id, '_stock_' . $i, wc_clean( $_POST[ '_stock_' . $i ] ) );

		}

		// update product stock
		update_post_meta( $post_id, '_stock', $stock );
	}
}

// display our locations 
function pt_wcit_echo_locations( $product ) {
	$i = $selected = 0;
	if ( isset( $_POST[ 'location' ] ) && $_POST[ 'location' ] )
		$selected = $_POST[ 'location' ];

	while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

		$i++;

		$location_enabled = get_option('pt_wcit_location_enabled_' . $i );
		if ( 'yes' != $location_enabled )
			continue;

		$location_name = get_option('pt_wcit_location_' . $i );
		if ( empty( $location_name ) )
			continue;

		echo '<option value="' . $i . '"';
		selected( $i, $selected );
		echo '>' . $location_name . '</option>';

	}

}
// display our locations and stock levels, excluding quantity already in cart
function pt_wcit_echo_locations_with_stock_levels( $product ) {

	if ( 'variation' == $product->product_type  ) {
		$id    = $product->variation_id;
		$field = '_variable_stock_';
	}else {
		$id    = $product->id;
		$field = '_stock_';
	}

	$i = $selected = 0;
	if ( isset( $_POST[ 'location' ] ) && $_POST[ 'location' ] )
		$selected = $_POST[ 'location' ];

	while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

		$i++;

		$stock         = get_post_meta( $id, $field . $i, true );
    if ( $stock < 1 )
        continue;
		$qty_in_cart   = pt_wcit_get_cart_item_quantity( $i );

		$current_stock = $stock - $qty_in_cart;

		$location_enabled = get_option('pt_wcit_location_enabled_' . $i );
		if ( 'yes' != $location_enabled )
			continue;

		$location_name = get_option('pt_wcit_location_' . $i );
		if ( empty( $location_name ) )
			continue;

		echo '<option value="' . $i . '"';
		if ( $current_stock < 1 )
			echo ' disabled="disabled"';
		selected( $i, $selected );
		echo '>' . $location_name . ' [' . sprintf( __( '%s in stock', 'woocommerce' ), $current_stock ) . ']</option>';

	}
}

// add our location fields div to admin screen
function pt_wcit_echo_location_fields_div( $product ) {
		echo '
<div class="inventory-location inventory-location-product-' . sanitize_title( $product->get_title() ) . '">
    <div class="form-row inventory-location-row">
    	<select name="location">
    		<option value="">' . __( 'Please select a location to ship from&hellip;', 'wcit-pluginterritory' ) . '</option>
    ';

	if ( $product->has_child() ) 
		pt_wcit_echo_locations( $product );
	else 
		pt_wcit_echo_locations_with_stock_levels( $product );

		echo '
		</select>
	</div>
	<div class="clear"></div>
</div>';
}

// add our location selector 
function pt_wcit_before_add_to_cart_button() {
	global $product;
	if ( $product->has_enough_stock( 1 ) ) 
		pt_wcit_echo_location_fields_div( $product );
}
add_action( 'woocommerce_before_add_to_cart_button', 'pt_wcit_before_add_to_cart_button', 20 );

// get item quantity in cart
function pt_wcit_get_cart_item_quantity( $item_location_id ) {
	$quantities = array();

	foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

		if ( isset( $values[ 'location_data' ] ) && $item_location_id == key( $values[ 'location_data' ] ) )
			return $values[ 'quantity' ];
	}

	return 0;
}

// validate info before adding to cart
function pt_wcit_add_to_cart_validation( $valid, $product_id, $quantity, $variation_id = 0, $variations = array(), $cart_item_data = array() ) {


	if ( empty( $_POST[ 'location' ] ) && ! isset( $cart_item_data[ 'location_data' ] ) ) {

		wc_add_notice( __( 'Please select a location to ship from below&hellip;', 'wcit-pluginterritory' ), 'error' );

	} else {

		if ( isset( $_POST[ 'location' ] ) ) {
			$location_id   = $_POST[ 'location' ];
			$location_name = get_option( 'pt_wcit_location_' . $location_id );
		} else {
			$location_id   = key( $cart_item_data[ 'location_data' ] );
			$location_name = current( $cart_item_data[ 'location_data' ] );
		}

		$stock = 0;
		if ( empty( $variation_id ) ) {
			$stock = get_post_meta( $product_id, '_stock_' . $location_id, true );
		} else {
			$stock = get_post_meta( $variation_id, '_variable_stock_' . $location_id, true );
		}

		if ( ! $stock ) {

				wc_add_notice( sprintf( __( 'There is no stock available at %s.', 'wcit-pluginterritory' ), $location_name ), 'error' );

		} elseif ( $quantity > $stock ) {

				wc_add_notice( sprintf( _n( 'You selected %s but there is only %s in stock at %s.', 'You selected %s but there are only %s in stock at %s.', $stock, 'wcit-pluginterritory' ), $quantity, $stock, $location_name ), 'error' );

		} else {

			$qty_in_cart = pt_wcit_get_cart_item_quantity( $location_id );
			if ( $quantity + $qty_in_cart  > $stock ) {

				if ( ($stock - $qty_in_cart) > 1 )
					wc_add_notice( sprintf( __( 'You cannot add %1$s to the cart &mdash; we have %2$s in stock at %3$s and you already have %4$s in your cart. Try adding %5$s.', 'wcit-pluginterritory' ), $quantity, $stock, $location_name, $qty_in_cart, $stock - $qty_in_cart  ), 'error' );
				else 
					wc_add_notice( sprintf( __( 'You cannot add %1$s to the cart &mdash; we have %2$s in stock at %3$s and you already have %4$s in your cart.', 'wcit-pluginterritory' ), $quantity, $stock, $location_name, $qty_in_cart  ), 'error' );

			}
		}

	}

	$valid = ( 0 == wc_notice_count( 'error' ) );
	return $valid;
}
add_filter( 'woocommerce_add_to_cart_validation', 'pt_wcit_add_to_cart_validation', 20, 6 );

// Load cart data 
function pt_wcit_get_cart_item_from_session( $cart_item, $values ) {
	if ( ! empty( $values[ 'location_data' ] ) ) {
		$cart_item[ 'location_data' ] = $values[ 'location_data' ];
	}
	return $cart_item;
}
add_filter( 'woocommerce_get_cart_item_from_session', 'pt_wcit_get_cart_item_from_session', 20, 2 );

// Add item data to the cart
function pt_wcit_add_cart_item_data( $cart_item_meta, $product_id, $post_data = null ) {

	if ( is_null( $post_data ) ) {
		$post_data = $_POST;
	}

	if ( empty( $cart_item_meta[ 'location_data' ] ) ) {
		$cart_item_meta[ 'location_data' ] = array();
	} else {
		$post_data[ 'location' ] = key( $cart_item_meta[ 'location_data' ] );
	}


	$location_id   = $post_data[ 'location' ];
	$location_name = get_option('pt_wcit_location_' . $location_id );

	$data                              = array( $location_id => $location_name );
	$cart_item_meta[ 'location_data' ] = $cart_item_meta[ 'location_data' ] + $data;

	return $cart_item_meta;
}
add_filter( 'woocommerce_add_cart_item_data', 'pt_wcit_add_cart_item_data', 20, 2 );

// check stock levels at cart
function pt_wcit_check_cart_items() {

	if ( wc_notice_count( 'error' ) )
		return false;

	foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
		if ( isset( $values[ 'location_data' ] ) ) {
	
			$location_id = key( $values[ 'location_data' ] );

			$_product     = $values['data'];

			if ( $_product->is_type( 'variation' ) && true === $_product->managing_stock() ) {
				// Variation has stock levels defined so its handled individually
				$stock = get_post_meta( $values[ 'variation_id' ], '_variable_stock_' . $location_id, true );
			} else {
				$stock = get_post_meta( $values[ 'product_id' ],   '_stock_' . $location_id, true );
			}

			if ( $stock < $values[ 'quantity' ] ) {
				$location_name = current( $values[ 'location_data' ] );
				wc_add_notice( sprintf( __( 'You cannot have %1$s in the cart &mdash; we have %2$s in stock at %3$s. Please edit your cart and try again. We apologise for any inconvenience caused.', 'wcit-pluginterritory' ), $values[ 'quantity' ], $stock, $location_name ), 'error' );
				return false;
			}
		}
	}
	return true;
}
add_action( 'woocommerce_check_cart_items', 'pt_wcit_check_cart_items' );

// when ordering again, set cart data 
function pt_wcit_add_order_item_data_to_cart( $cart_item_meta, $item ) {
	
	$item_meta_data = $item->get_meta_data();
	foreach ( $item->get_meta_data() as $meta ) {

		if ( '_order_location_data' == $meta->key ) {

			$location_data = $meta->value;

			if ( isset( $cart_item_meta[ 'location_data' ] ) )
				$cart_item_meta[ 'location_data' ] = array_merge( $cart_item_meta[ 'location_data' ], $location_data );
			else 
				$cart_item_meta[ 'location_data' ] = $location_data;

		}

	}
	return $cart_item_meta;
}
add_filter( 'woocommerce_order_again_cart_item_data', 'pt_wcit_add_order_item_data_to_cart', 20, 2 );

// Display item Location at the cart
function pt_wcit_get_item_data( $other_data, $cart_item ) {
	if ( ! empty( $cart_item[ 'location_data' ] ) ) {
		foreach ( $cart_item[ 'location_data' ] as $key => $value) {
			$other_data[] = array(
				'name'    => __('Location', 'wcit-pluginterritory' ),
				'value'   => $value,
			);
		}
	}
	return $other_data;
}
add_filter( 'woocommerce_get_item_data', 'pt_wcit_get_item_data', 20, 2 );

// Add meta to order
function pt_wcit_order_item_meta( $item_id, $values ) {
	if ( ! empty( $values[ 'location_data' ] ) ) 
		foreach ( $values[ 'location_data' ] as $key => $value) {
			woocommerce_add_order_item_meta( $item_id, __( 'Location', 'wcit-pluginterritory' ), $value );
			woocommerce_add_order_item_meta( $item_id, '_order_location_data',          array( $key => $value ) );
		}
}
add_action( 'woocommerce_add_order_item_meta', 'pt_wcit_order_item_meta', 20, 2 );

// Manage reducing stock
function pt_wcit_reduce_order_stock( $order ) {

	foreach ( $order->get_items() as $item ) {

		$_product = $item->get_product();

		if ( $_product && $_product->exists() && $_product->managing_stock() ) {

			$item_meta_data = $item->get_meta_data();
			foreach ( $item->get_meta_data() as $meta ) {
				if ( '_order_location_data' == $meta->key ) {

					$qty           = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $order, $item );

					$location_data = $meta->value;
					$location_id   = key( $location_data );
					$location_name = current( $location_data );

					$product_id    = $_product->get_id();

					if ( 'variation' == $_product->get_type() ) {

						$stock = get_post_meta( $product_id, '_variable_stock_' . $location_id, true );
						$new_stock = $stock - $qty;
						update_post_meta( $product_id, '_variable_stock_' . $location_id, $new_stock );
						$order->add_order_note( sprintf( __( 'Location `%s` variation #%s stock reduced from %s to %s.', 'woocommerce' ), $location_name, $product_id, $stock, $new_stock ) );

					} else {

						$stock = get_post_meta( $product_id, '_stock_' . $location_id, true );
						$new_stock = $stock - $qty;
						update_post_meta( $product_id, '_stock_' . $location_id, $new_stock );
						$order->add_order_note( sprintf( __( 'Location `%s` item #%s stock reduced from %s to %s.', 'woocommerce' ), $location_name, $product_id, $stock, $new_stock ) );

					}

					$order->send_stock_notifications( $_product, $new_stock, $item['qty'] );
				}
			}
		}
	}
}
add_action( 'woocommerce_reduce_order_stock', 'pt_wcit_reduce_order_stock' );

// display availabilty per location
function pt_wcit_get_availability( $availability_html, $availability, $product = null ) {

	if ( is_null( $product ) ) {
		return $availability_html;
	}

	$i                 = 0;
	$availability_html = '';
	$id                = $product->get_id();

	if ( 'variation' == $product->get_type()  ) {
		$field = '_variable_stock_';
	}else {
		$field = '_stock_';
	}

	while ( $i < get_option( 'pt_wcit_number_of_inventory_locations' )  ) {

		$i++;

		$location_enabled = get_option('pt_wcit_location_enabled_' . $i );
		if ( 'yes' != $location_enabled )
			continue;

		$location_name = get_option('pt_wcit_location_' . $i );
		if ( empty( $location_name ) )
			continue;

		$stock         = get_post_meta( $id, $field . $i, true );
    if ( $stock < 1 )
        continue;
		$qty_in_cart   = pt_wcit_get_cart_item_quantity( $i );

		$current_stock = $stock - $qty_in_cart;

		if ( $current_stock < 1 )
			$class = 'out-of-stock';
		else 
			$class = 'in-stock';

		$availability_html .=  '
			<li class="stock ' . $class . '">' . $location_name . ':  ' . sprintf( __( '%s in stock', 'woocommerce' ), $current_stock ) . '</li>';

	}

	return '<hr><div class="stock">
<span style="color: #005670;"><strong><span class="location-header">NOW YOU CAN ORDER DIRECT FROM OUR STORES OR WAREHOUSE!
</span></strong></span><br>
<span style="color: #e2401c;"><strong><span class="location-header">These are the locations you can ship from:
</span></strong></span><br><span style="color: #e2401c;"><span style="font-size: small;"></span></span><ul>' . $availability_html . '</ul></div>';
}
add_filter( 'woocommerce_stock_html', 'pt_wcit_get_availability', 10, 3 );
