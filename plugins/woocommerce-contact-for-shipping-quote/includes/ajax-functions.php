<?php
namespace WooCommerce_Contact_for_Shipping_Quote;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


function request_shipping_quote() {
	check_ajax_referer( 'contact-for-shipping-quote', 'nonce' );

	$post_data = isset( $_POST['post_data'] ) ? wp_unslash( $_POST['post_data'] ) : '';
	$post_data = wp_parse_args( $post_data );

	// Check for field requirements
	$required_data = get_option( 'shipping_quote_required_data', array() );
	if ( ! empty( $required_data ) ) {

		$missing_fields = array();
		foreach ( $required_data as $k ) {

			if ( method_exists( WC()->customer, 'get_shipping_' . $k ) ) {
				$v = call_user_func( array( WC()->customer, 'get_shipping_' . $k ) );
			} elseif ( $k === 'email' ) {
				$v = WC()->customer->get_billing_email() ?: $_POST['billing_email'] ?: $post_data['billing_email'] ?? '';
			}

			if ( empty( $v ) ) {
				$missing_fields[] = $k;
			}
		}

		if ( ! empty( $missing_fields ) ) {
			if ( $_POST['cart'] ) {
				wc_add_notice( 'Not all required data is available to request a shipping quote. Please enter your information at the checkout and try again.', 'error' );
			} else {
				foreach ( $missing_fields as $k ) {
					wc_add_notice( sprintf( __( '%s is a required field to request a shipping quote', 'woocommerce-contact-for-shipping-quote' ), '<strong>' . $k . '</strong>' ), 'error' );
				}
			}

			wp_send_json( array(
				'success' => false,
				'notice' => wc_print_notices( true ),
			) );
			die;
		}
	}

	$address_hash = get_address_hash();
	$cart_hash = get_cart_hash();

	// Try to get a existing quote
	$quote = Shipping_Quote::read_by( array( 'address_hash' => $address_hash, 'cart_hash' => $cart_hash, 'status' => array( 'new', 'pending' ) ), array( '%s', '%s', '%s' ) );

	// If not available, create one.
	if ( ! $quote ) {
		$email = WC()->checkout()->get_value( 'billing_email' ) ?: (isset( $post_data['billing_email'] ) ? $post_data['billing_email'] : '');

		$quote = Shipping_Quote::create( array(
			'customer_email' => $email,
		) );

		WC()->mailer()->get_emails(); // Init WC Emails to add trigger
		do_action( 'WCCSQ/requested_shipping_quote', $quote );
	}

	wp_send_json( array(
		'success' => $quote !== false,
		'cart_hash' => $quote->get_cart_hash(),
		'address_hash' => $quote->get_address_hash(),
	) );
}
add_action( 'wp_ajax_wccsq_request_shipping_quote', '\WooCommerce_Contact_for_Shipping_Quote\request_shipping_quote' );
add_action( 'wp_ajax_nopriv_wccsq_request_shipping_quote', '\WooCommerce_Contact_for_Shipping_Quote\request_shipping_quote' );
