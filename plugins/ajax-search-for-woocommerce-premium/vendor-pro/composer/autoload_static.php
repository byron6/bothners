<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit61a1199b616d9f22d0e94d4c4386516d
{
    public static $files = array (
        'b0d3065f54be7e8230de3f07effd5eab' => __DIR__ . '/..' . '/damian-gora/tntsearch/helper/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'TeamTNT\\TNTSearchASFW\\' => 22,
        ),
        'D' => 
        array (
            'DgoraWcas\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'TeamTNT\\TNTSearchASFW\\' => 
        array (
            0 => __DIR__ . '/..' . '/damian-gora/tntsearch/src',
        ),
        'DgoraWcas\\' => 
        array (
            0 => __DIR__ . '/../..'.'/composer-pro' . '/../includes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'WP_Async_Request' => __DIR__ . '/..' . '/a5hleyrich/wp-background-processing/classes/wp-async-request.php',
        'WP_Background_Process' => __DIR__ . '/..' . '/a5hleyrich/wp-background-processing/classes/wp-background-process.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit61a1199b616d9f22d0e94d4c4386516d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit61a1199b616d9f22d0e94d4c4386516d::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit61a1199b616d9f22d0e94d4c4386516d::$classMap;

        }, null, ClassLoader::class);
    }
}
