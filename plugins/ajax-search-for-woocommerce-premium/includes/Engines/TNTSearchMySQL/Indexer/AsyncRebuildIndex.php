<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AsyncRebuildIndex extends \WP_Async_Request {
	/**
	 * Prefix
	 * @var string
	 */
	protected $prefix = 'wcas';

	/**
	 * @var string
	 */
	protected $action = 'async_rebuild_index';

	/**
	 * Handle
	 */
	protected function handle() {

		if ( Builder::getInfo( 'status' ) === 'completed' ) {
			Builder::buildIndex();
		}

	}
}
