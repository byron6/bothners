<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$birthday_field_pages_to_show = array(
	'my-account'    => esc_html__( 'My Account Page', 'yith-woocommerce-points-and-rewards' ),
	'register_form' => esc_html__( 'Registration Form', 'yith-woocommerce-points-and-rewards' ),
	'checkout'      => esc_html__( 'Checkout Page', 'yith-woocommerce-points-and-rewards' ),
);

$tab = array(
	'points-extra' => array(

		/* EXTRA POINTS REVIEWS */
		'extra_points_review_title'         => array(
			'name' => esc_html__( 'Extra Points for review', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_extra_points_review_title',
		),

		'enable_review_exp'                 => array(
			'name'      => esc_html__( 'Assign points to users that leave a product review', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to user that leave a product review', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_review_exp',
		),

		'review_exp'                        => array(
			'name'                   => esc_html__( 'Points to assign for reviews', 'yith-woocommerce-points-and-rewards' ),
			'desc'                   => esc_html__( 'Set how many points to assign for reviews. Check "repeat" to repeat the rules: if you set 5 reviews = 10 points and check repeat 10 reviews will be 20 points and so on.', 'yith-woocommerce-points-and-rewards' ),
			'type'                   => 'yith-field',
			'yith-type'              => 'options-extrapoints',
			'label'                  => esc_html__( 'Review(s) is equivalent to', 'yith-woocommerce-points-and-rewards' ),
			'default'                => array(
				'list' => array(
					array(
						'number' => 1,
						'points' => 10,
						'repeat' => 0,
					),
				),
			),
			'id'                     => 'ywpar_review_exp',
			'deps'                   => array(
				'id'    => 'ywpar_enable_review_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
			'yith-sanitize-callback' => 'ywpar_option_extrapoints_sanitize',
		),


		'extra_points_review_title_end'     => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_extra_points_review_title_end',
		),

		'extra_num_order_title'             => array(
			'name' => esc_html__( 'Extra points for orders', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_extra_num_order_title',
		),

		'enable_num_order_exp'              => array(
			'name'      => esc_html__( 'Assign points based on orders placed', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to a user, when an order is placed.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_num_order_exp',
		),

		'num_order_exp'                     => array(
			'name'                   => esc_html__( 'Points to assign per order', 'yith-woocommerce-points-and-rewards' ),
			'desc'                   => esc_html__( 'Set how many points to assign per order placed', 'yith-woocommerce-points-and-rewards' ),
			'type'                   => 'yith-field',
			'yith-type'              => 'options-extrapoints',
			'label'                  => esc_html__( 'order(s) =', 'yith-woocommerce-points-and-rewards' ),
			'default'                => array(
				'list' => array(
					array(
						'number' => 1,
						'points' => 10,
						'repeat' => 0,
					),
				),
			),
			'id'                     => 'ywpar_num_order_exp',
			'deps'                   => array(
				'id'    => 'ywpar_enable_num_order_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
			'yith-sanitize-callback' => 'ywpar_option_extrapoints_sanitize',
		),

		'extra_num_order_title_end'         => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_extra_num_order_title_end',
		),

		'amount_spent_title'                => array(
			'name' => esc_html__( 'Extra points for amount spent', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_amount_spent_title',
		),

		'enable_amount_spent_exp'           => array(
			'name'      => esc_html__( 'Assign points based on the total amount spent', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to a user, when an order is placed.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_amount_spent_exp',
		),


		'amount_spent_exp'                  => array(
			'name'      => esc_html__( 'Points to assign per amount reached', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Set how many points to allocate for each amount reached', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'options-extrapoints',
			'label'     => sprintf( '%s (%s)', get_woocommerce_currency_symbol(), get_woocommerce_currency() ) . __( ' spent =', 'yith-woocommerce-points-and-rewards' ),
			'default'   => array(
				'list' => array(
					array(
						'number' => 1000,
						'points' => 10,
						'repeat' => 0,
					),
				),
			),
			'id'        => 'ywpar_amount_spent_exp',
			'deps'      => array(
				'id'    => 'ywpar_enable_amount_spent_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'amount_spent_title_end'            => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_amount_spent_title_end',
		),


		'checkout_threshold_title'          => array(
			'name' => esc_html__( 'Extra Points for cart total', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_checkout_threshold_title',
		),

		'enable_checkout_threshold_exp'     => array(
			'name'      => esc_html__( 'Assign points based on the cart total', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to user, based on the cart total', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_checkout_threshold_exp',
		),


		'checkout_threshold_exp'            => array(
			'name'        => esc_html__( 'Points to assign on cart total', 'yith-woocommerce-points-and-rewards' ),
			'desc'        => esc_html__( 'Set how many points to assign based on cart total', 'yith-woocommerce-points-and-rewards' ),
			'type'        => 'yith-field',
			'yith-type'   => 'options-extrapoints',
			'label'       => sprintf( '%s (%s)', get_woocommerce_currency_symbol(), get_woocommerce_currency() ) . __( ' spent =', 'yith-woocommerce-points-and-rewards' ),
			'default'     => array(
				'list' => array(
					array(
						'number' => 1000,
						'points' => 10,
						'repeat' => 0,
					),
				),
			),
			'show_repeat' => false,
			'id'          => 'ywpar_checkout_threshold_exp',
			'deps'        => array(
				'id'    => 'ywpar_enable_checkout_threshold_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'checkout_threshold_not_cumulate'   => array(
			'name'      => esc_html__( 'Based on cart total, allow user to achieve multiple thresholds', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'If you created multiple points based rules on cart total, choose whether to apply all rules or just the highest value rule.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'default'   => 'yes',
			'options'   => array(
				'no'  => esc_html__( 'Apply all rules and sum up the points', 'yith-woocommerce-points-and-rewards' ),
				'yes' => esc_html__( 'Apply only the rule with the highest number of points in cart', 'yith-woocommerce-points-and-rewards' ),
			),
			'id'        => 'ywpar_checkout_threshold_not_cumulate',
			'deps'        => array(
				'id'    => 'ywpar_enable_checkout_threshold_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'checkout_threshold_show_message'   => array(
			'name'      => esc_html__( 'Show threshold message in cart and checkout', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to show messages about thresholds achieved on Cart & Checkout pages', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_checkout_threshold_show_message',
			'deps'        => array(
				'id'    => 'ywpar_enable_checkout_threshold_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'checkout_threshold_title_end'      => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_checkout_threshold_title_end',
		),



		'number_of_points_title'            => array(
			'name' => esc_html__( 'Enable points incentive on total collected points', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_number_of_points_title',
		),

		'enable_number_of_points_exp'       => array(
			'name'      => esc_html__( 'Assign extra points based on the total number of points collected', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points to user based on the points he collected', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_number_of_points_exp',
		),

		'number_of_points_exp'              => array(
			'name'      => esc_html__( 'Points to assign for points collected', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Set how many points to assign based on total of points collected', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'options-extrapoints',
			'label'     => esc_html__( 'points =', 'yith-woocommerce-points-and-rewards' ),
			'default'   => array(
				'list' => array(
					array(
						'number' => 1,
						'points' => 10,
						'repeat' => 0,
					),
				),
			),
			'id'        => 'ywpar_number_of_points_exp',
			'deps'      => array(
				'id'    => 'ywpar_enable_number_of_points_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'number_of_points_title_end'        => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_number_of_points_title_end',
		),


		'point_on_registration_title'       => array(
			'name' => esc_html__( 'Extra points for user registration', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_point_on_registration_title_title',
		),

		'enable_points_on_registration_exp' => array(
			'name'      => esc_html__( 'Assign points when a user registers', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Assign extra points to newly registered users', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_points_on_registration_exp',
		),

		'points_on_registration'            => array(
			'name' => esc_html__( 'Points to assign to new users', 'yith-woocommerce-points-and-rewards' ),
			'desc'              => __( 'Set how many points to assing to new users', 'yith-woocommerce-points-and-rewards' ),
			'type'              => 'yith-field',
			'yith-type'         => 'number',
			'default'           => 1,
			'step'              => 1,
			'min'               => 1,
			'custom_attributes' => 'style="width:70px"',
			'id'                => 'ywpar_points_on_registration',
			'deps'              => array(
				'id'    => 'ywpar_enable_points_on_registration_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'point_on_registration_title_end'   => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_point_on_registration_title_end',
		),


		'point_on_birthday_title'           => array(
			'name' => esc_html__( 'Extra points for users\' birthday', 'yith-woocommerce-points-and-rewards' ),
			'type' => 'title',
			'id'   => 'ywpar_point_on_registration_title',
		),

		'enable_points_on_birthday_exp'     => array(
			'name'      => esc_html__( 'Assign extra points on customer\'s birthday', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Enable to assign points on customers birthday', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
			'id'        => 'ywpar_enable_points_on_birthday_exp',
		),

		'points_on_birthday'                => array(
			'name'              => esc_html__( 'Points to assign per birthday', 'yith-woocommerce-points-and-rewards' ),
			'desc'              => esc_html__( 'Set how many points to assign to users on their birthday', 'yith-woocommerce-points-and-rewards' ),
			'type'              => 'yith-field',
			'yith-type'         => 'number',
			'default'           => 1,
			'step'              => 1,
			'min'               => 1,
			'custom_attributes' => 'style="width:70px"',
			'id'                => 'ywpar_points_on_birthday',
			'deps'              => array(
				'id'    => 'ywpar_enable_points_on_birthday_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'birthday_date_field_where'         => array(
			'name'      => esc_html__( 'Show birthday input field in', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose where to show the Birthday Date Field.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'checkbox-array',
			'multiple'  => true,
			'options'   => $birthday_field_pages_to_show,
			'default'   => array( 'my-account', 'register_form', 'checkout' ),
			'id'        => 'ywpar_birthday_date_field_where',
			'deps'      => array(
				'id'    => 'ywpar_enable_points_on_birthday_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'birthday_date_format'              => array(
			'name'      => esc_html__( 'Birthday input date format', 'yith-woocommerce-points-and-rewards' ),
			'desc'      => esc_html__( 'Choose the format to use for the Birthday Date Field.', 'yith-woocommerce-points-and-rewards' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'class'     => 'wc-enhanced-select',
			'options'   => ywpar_date_placeholders(),
			'default'   => 'yy-mm-dd',
			'id'        => 'ywpar_birthday_date_format',
			'deps'      => array(
				'id'    => 'ywpar_enable_points_on_birthday_exp',
				'value' => 'yes',
				'type'  => 'hide',
			),
		),

		'point_on_birthday_title_end'       => array(
			'type' => 'sectionend',
			'id'   => 'ywpar_point_on_birthday_title_end',
		),
	)
);

return $tab;