<?php

if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_YWPAR_VERSION' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Manage plugin versions compatibility
 *
 * @class   YITH_WC_Points_Rewards_Version_Compatibility
 * @package YITH WooCommerce Points and Rewards
 * @since   2.0.0
 * @author  YITH
 */
if ( ! class_exists( 'YITH_WC_Points_Rewards_Version_Compatibility' ) ) {

	/**
	 * Class YITH_WC_Points_Rewards_Version_Compatibility
	 */
	class YITH_WC_Points_Rewards_Version_Compatibility {
		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WC_Points_Rewards
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WC_Points_Rewards_Version_Compatibility
		 * @since 2.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize version compatibility class
		 *
		 * @since  2.0.0
		 * @author Armando Liccardo
		 */
		public function __construct() {

			// before version 2.0.0.
			if ( version_compare( get_option( 'ywpar_db_version',0), '2.0.0', '<' ) ) {
				$this->compatibility_to_version_200();
			}

		}

		private function compatibility_to_version_200 () {
			error_log('YWPAR Compatibility actions');
			update_option( 'ywpar_db_version', '2.0.0' );

			/* Assign Points to option in Points Options > Standard Points */
			$ywpar_user_role_enabled_type_value = get_option( 'ywpar_user_role_enabled', 'all' );
			if ( count( $ywpar_user_role_enabled_type_value ) > 0 && is_array( $ywpar_user_role_enabled_type_value ) && in_array( 'all', $ywpar_user_role_enabled_type_value ) ) {
				update_option( 'ywpar_user_role_enabled_type', 'all');
			} else {
				update_option( 'ywpar_user_role_enabled_type', 'roles');
			}

			/* Expiration field option in Points Options > Standard Points */
			/* from int to array */
			$exp_days = get_option( 'ywpar_days_before_expiration', 0 );
			$expire_value = array(
				'number' => $exp_days,
				'time' => 'days'
			);
			update_option( 'ywpar_days_before_expiration', maybe_serialize( $expire_value ) );
			if ( '' === $exp_days || 0 === intval($exp_days) ) {
				update_option( 'ywpar_enable_expiration_point', 'no' );
			}


			/* User that can redeem points option in Points Options > Standard Points */
			$ywpar_user_role_redeem_enabled_type_value = get_option( 'ywpar_user_role_redeem_enabled', 'all' );
			if ( count( $ywpar_user_role_redeem_enabled_type_value ) > 0 && is_array( $ywpar_user_role_redeem_enabled_type_value ) && in_array( 'all', $ywpar_user_role_redeem_enabled_type_value ) ) {
				update_option( 'ywpar_user_role_redeem_type', 'all');
			} else {
				update_option( 'ywpar_user_role_redeem_type', 'roles');
			}

			/* Apply Redeeming Restrictions option */
			$apply_redeem_restrictions = 'yes';
			if ( '' === get_option( 'ywpar_max_points_discount', '' ) && '' === get_option('ywpar_minimum_amount_to_redeem', '') && '' === get_option('ywpar_minimum_amount_discount_to_redeem', '') && '' === get_option('ywpar_max_points_product_discount', '') ) {
				$apply_redeem_restrictions = 'no';
			}

			if ( '' === get_option( 'ywpar_max_percentual_discount', '') && '' === get_option( 'ywpar_minimum_amount_to_redeem', '') ) {
				$apply_redeem_restrictions = 'no';
			}
			update_option( 'ywpar_apply_redeem_restrictions', $apply_redeem_restrictions );

			update_option('ywpar_enabled_rewards_cart_message_layout_style', 'default' );
			if ( get_option( 'ywpar_enabled_rewards_cart_message' ) === 'yes' ) {
				/* use old style for reward message */
				update_option('ywpar_enabled_rewards_cart_message_layout_style', 'custom' );
			}
		}
	}
}

if ( !function_exists( 'ywpar_give_zero_points_to_users_with_no_points' ) ) {
	function ywpar_give_zero_points_to_users_with_no_points() {
		$args = array(
			'number' => -1,
		);
		$users =  new WP_User_Query( $args );

		foreach ( $users->get_results() as $u ) {
			if ( '' === get_user_meta( $u->ID, '_ywpar_user_total_points', true ) ) {
				update_user_meta( $u->ID, '_ywpar_user_total_points', 0 );
			}
		}
	}
}
/**
 * Unique access to instance of YITH_WC_Points_Rewards class
 *
 * @return \YITH_WC_Points_Rewards_Version_Compatibility
 */
function YITH_WC_Points_Rewards_Version_Compatibility() {
	return YITH_WC_Points_Rewards_Version_Compatibility::get_instance();
}