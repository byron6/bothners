<?php
class apiCall {

    private $url;

    function __construct() {
        $this->url = 'https://xc7yq5c7jluyl47rqmay.kerridgecs.co.za/XI/Core';
    }

    public function GetAccountDetails($method, $body) {

        $url = $this->url.'/'.$method;
        $xml_post_string = $body;
		
		$curl = curl_init();
	
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/xml',
		));
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xml_post_string);
	
		$result = new SimpleXMLElement(curl_exec($curl));
		curl_close($curl);

        return $result;

    }
}
?>