<?php
/**
 * @link              https://www.semantica.co.za/
 * @since             1.0.0
 * @package           Semantica Core
 *
 * @wordpress-plugin
 * Plugin Name:       Semantica Core
 * Plugin URI:        https://www.semantica.co.za/
 * Description:       Semantica Core Integrations
 * Version:           1.0.0
 * Author:            Semantica
 * Author URI:        https://www.semantica.co.za/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

define( 'SEM_PATH', plugin_dir_path( __FILE__ ) );

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Enque Scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
function sem_required_scripts() {
    wp_register_style( 'sem_css', plugins_url('css/semantica-core.css', __FILE__) );
    wp_enqueue_style( 'sem_css' );
	wp_register_script('sem_jquery', plugins_url('js/semantica-core.js', __FILE__),array('jquery'),'', true);
    wp_enqueue_script('sem_jquery');
	wp_register_script('sem_slick', plugins_url('js/slick.js', __FILE__),array('jquery'),'', true);
    wp_enqueue_script('sem_slick');
	wp_register_style( 'sem_slick_css', plugins_url('css/slick.css', __FILE__) );
    wp_enqueue_style( 'sem_slick_css' );
	wp_register_style( 'sem_slick_theme_css', plugins_url('css/slick-theme.css', __FILE__) );
    wp_enqueue_style( 'sem_slick_theme_css' );
    wp_localize_script( 'sem_jquery', 'asc_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'sem_required_scripts' );

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Includes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
require_once ( SEM_PATH . '/kerridge/api.php');
require_once ( SEM_PATH . '/shortcodes.php');

//Make Kerridge API Calls
function kerridge_api(){
	
	$apiCall = new apiCall();
	
	$request = array(
		$_REQUEST['api_acc_num'] => 'account'
	);
	
	if($_REQUEST['api_method'] == 'GetBranches'){
		$request = array(
			'' => 'branch'
		);
	}
	
	$xml = new SimpleXMLElement('<root/>');
	array_walk_recursive($request, array ($xml, 'addChild'));
	$body = $xml->asXML();
	
	//echo '<pre>'.print_r($body, true).'</pre>';
	if($_REQUEST['api_method'] == 'GetBranches'){
		$body = '
		<root>
			<branch></branch>
		</root>
		';
	}
	
	if($_REQUEST['api_method'] == 'GetAccountHistory'){
		$body = '
		<root>
			<account>'.$_REQUEST['api_acc_num'].'</account>
			<filter>
				<year>2020</year>
				<period>12</period>
				<datefrom>2020-01-01</datefrom>
				<dateto>2020-12-30</dateto>
				<maxrecs>9000</maxrecs>
			</filter>
		</root>
		';
	}
	
	if($_REQUEST['api_method'] == 'GetInvoices'){
		$body = '
		<root>
			<account>'.$_REQUEST['api_acc_num'].'</account>
		</root>
		';
	}
	
	$response = $apiCall->GetAccountDetails($_REQUEST['api_method'], $body);
	
	//$_REQUEST['api-method']
	echo '<pre>'.print_r($response, true).'</pre>';
 
	die();
}
add_action('wp_ajax_kerridge_api', 'kerridge_api'); 
add_action('wp_ajax_nopriv_kerridge_api', 'kerridge_api');