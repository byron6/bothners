<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$single_product_message_icon = '<img style="max-width: 16px; margin-right: 5px;" src="' . YITH_YWPAR_ASSETS_URL . '/images/ywpar_message.svg" />';

$section1 = array(
	'general_title'                       => array(
		'name' => __( 'General options', 'yith-woocommerce-points-and-rewards' ),
		'type' => 'title',
		'id'   => 'ywpar_general_option',
	),

	'enabled_shop_manager'                => array(
		'name'      => esc_html__( 'Allow shop manager to manage this plugin', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to allow your shop manager to access and manage the plugin settings.', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'no',
		'id'        => 'ywpar_enabled_shop_manager',
	),

	'hide_point_system_to_guest'          => array(
		'name'      => esc_html__( 'Hide points messages to guest users', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to hide point messages to guest users.', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'yes',
		'id'        => 'ywpar_hide_point_system_to_guest',
	),

	'general_settings_end'                => array(
		'type' => 'sectionend',
		'id'   => 'ywpar_general_option_end',
	),
	// MESSAGE IN LOOP
	'points_in_shop_pages'                => array(
		'name' => esc_html__( 'Points in shop pages', 'yith-woocommerce-points-and-rewards' ),
		'type' => 'title',
		'id'   => 'ywpar_points_in_shop_pages',
	),

	'enabled_loop_message'             => array(
		'name'      => esc_html__( 'Show points message in shop pages (loop)', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to show the message related to points in all shop pages', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'no',
		'id'        => 'ywpar_enabled_loop_message',
	),

	'loop_message'                     => array(
		'name'      => esc_html__( 'Loop Message', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => _x( '{points} number of points earned;<br>{points_label} of points;<br>{price_discount_fixed_conversion} the value corresponding to points','do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
		'yith-type' => 'textarea-editor',
		'type'      => 'yith-field',
		'default'   => _x( '<strong>{points}</strong> {points_label}', 'do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
		'id'        => 'ywpar_loop_message',
		'deps'      => array(
			'id'    => 'ywpar_enabled_loop_message',
			'value' => 'yes',
			'type'  => 'hide',
		),
		'textarea_rows' => 5
	),

	//SINGLE PRODUCT PAGE
	'enabled_single_product_message'   => array(
		'name'      => esc_html__( 'Show points message in product page', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to show the message related to points in all product pages', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'yes',
		'id'        => 'ywpar_enabled_single_product_message',
	),

	'single_product_message_position'  => array(
		'name'      => esc_html__( 'Message position', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Choose where to show the points message in the product page', 'yith-woocommerce-points-and-rewards' ),
		'yith-type' => 'select',
		'class'     => 'wc-enhanced-select',
		'type'      => 'yith-field',
		'options'   => array(
			'before_add_to_cart' => esc_html__( 'Before "Add to cart" button', 'yith-woocommerce-points-and-rewards' ),
			'after_add_to_cart'  => esc_html__( 'After "Add to cart" button', 'yith-woocommerce-points-and-rewards' ),
			'before_excerpt'     => esc_html__( 'Before excerpt', 'yith-woocommerce-points-and-rewards' ),
			'after_excerpt'      => esc_html__( 'After excerpt', 'yith-woocommerce-points-and-rewards' ),
			'after_meta'         => esc_html__( 'After product meta', 'yith-woocommerce-points-and-rewards' ),
		),
		'default'   => 'before_add_to_cart',
		'id'        => 'ywpar_single_product_message_position',
		'deps'      => array(
			'id'    => 'ywpar_enabled_single_product_message',
			'value' => 'yes',
			'type'  => 'hide',
		),
	),

	'single_product_message'           => array(
		'name'      => esc_html__( 'Single Product Page Message', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => _x( '{points} number of points earned;<br>{points_label} of points;<br>{price_discount_fixed_conversion} the value corresponding to points ','do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
		'yith-type' => 'textarea-editor',
		'type'      => 'yith-field',
		'default'   => $single_product_message_icon . _x( 'If you purchase this product you will earn <strong>{points}</strong> {points_label} worth of {price_discount_fixed_conversion}!','do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
		'id'        => 'ywpar_single_product_message',
		'deps'      => array(
			'id'    => 'ywpar_enabled_single_product_message',
			'value' => 'yes',
			'type'  => 'hide',
		),
		'textarea_rows' => 5
	),

	'single_product_points_message_colors' => array(
		'id' => 'ywpar_single_product_points_message_colors',
		'type'         => 'yith-field',
		'yith-type'    => 'multi-colorpicker',
		'name'      => esc_html__( 'Colors', 'yith-woocommerce-points-and-rewards' ),
		'colorpickers'  => array(
							array(
								'id' => 'text_color',
								'name' => esc_html__( 'Text', 'yith-woocommerce-points-and-rewards' ),
								'default' => '#000000'
								),
							array(
							   'id' => 'background_color',
							   'name' => esc_html__( 'Background', 'yith-woocommerce-points-and-rewards' ),
							   'default' => 'rgba(255,255,255,0)'
							   )
							),
		'deps'      => array(
				'id'    => 'ywpar_enabled_single_product_message',
				'value' => 'yes',
				'type'  => 'hide',
		),					
	),

	'points_in_shop_pages_end'                    => array(
		'type' => 'sectionend',
		'id'   => 'ywpar_points_in_shop_pages',
	),

	// MY ACCOUNT PAGE
	'show_points_in_myaccount_options'                        => array(
		'name' => esc_html__( 'Points in My Account', 'yith-woocommerce-points-and-rewards' ),
		'type' => 'title',
		'id'   => 'ywpar_show_options',
	),


	'show_point_list_my_account_page'     => array(
		'name'      => esc_html__( 'Show points on My Account', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to show points in the "My Account" page of all users', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'yes',
		'id'        => 'ywpar_show_point_list_my_account_page',
	),

	'my_account_page_label'               => array(
		'name'      => esc_html__( 'Label for points section', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enter a label to identify the points section in My Account page', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'text',
		'default'   => esc_html__( 'My Points', 'yith-woocommerce-points-and-rewards' ),
		'id'        => 'ywpar_my_account_page_label',
		'deps'      => array(
			'id'    => 'ywpar_show_point_list_my_account_page',
			'value' => 'yes',
			'type'  => 'hide',
		),
	),

	'my_account_page_endpoint'            => array(
		'name'      => esc_html__( 'Endpoint for points section', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enter the endpoint of the Points in My account page. Endpoints cannot contain any spaces nor uppercase letters.', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'text',
		'default'   => 'my-points',
		'id'        => 'ywpar_my_account_page_endpoint',
		'deps'      => array(
			'id'    => 'ywpar_show_point_list_my_account_page',
			'value' => 'yes',
			'type'  => 'hide',
		),
	),

	'show_points_worth_money'             => array(
		'name'      => esc_html__( 'Show Points Value', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'enable to show the points value. Example: "You have 100 points - 15$', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'yes',
		'id'        => 'ywpar_show_point_worth_my_account',
		'deps'      => array(
			'id'    => 'ywpar_show_point_list_my_account_page',
			'value' => 'yes',
			'type'  => 'hide',
		),

	),

	'show_point_summary_on_order_details' => array(
		'name'      => esc_html__( 'Show points earned and spent', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Show points earned and spent in My Account > Order details', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'no',
		'id'        => 'ywpar_show_point_summary_on_order_details',
	),
	'show_point_summary_on_email'         => array(
		'name'      => __( '', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => __( 'Show points earned and spent in the email of Order completed', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'no',
		'id'        => 'ywpar_show_point_summary_on_email',
	),
	'show_options_end'                    => array(
		'type' => 'sectionend',
		'id'   => 'ywpar_show_options',
	),

	// CART & CHECKOUT PAGES
	'points_in_cart_checkout_pages'                => array(
		'name' => esc_html__( 'Points in Cart & Checkout', 'yith-woocommerce-points-and-rewards' ),
		'type' => 'title',
		'id'   => 'ywpar_points_in_cart_checkout_pages',
	),

	'enabled_cart_message'             => array(
		'name'      => esc_html__( 'Show points in Cart page', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to show the points message in cart page', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'yes',
		'id'        => 'ywpar_enabled_cart_message',
	),

	'cart_message'                     => array(
		'name'      => esc_html__( 'Message text in cart', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enter the text to show in cart page', 'yith-woocommerce-points-and-rewards' ),
		'yith-type' => 'textarea-editor',
		'type'      => 'yith-field',
		'default'   => _x( 'If you proceed to checkout, you will earn <strong>{points}</strong> {points_label}!', 'do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
		'id'        => 'ywpar_cart_message',
		'deps'      => array(
			'id'    => 'ywpar_enabled_cart_message',
			'value' => 'yes',
			'type'  => 'hide',
		),
		'textarea_rows' => 5
	),

	'enabled_checkout_message'         => array(
		'name'      => esc_html__( 'Show points in Checkout page', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enable to show the points message in checkout page', 'yith-woocommerce-points-and-rewards' ),
		'type'      => 'yith-field',
		'yith-type' => 'onoff',
		'default'   => 'yes',
		'id'        => 'ywpar_enabled_checkout_message',
	),

	'checkout_message'                 => array(
		'name'      => esc_html__( 'Message text in checkout', 'yith-woocommerce-points-and-rewards' ),
		'desc'      => esc_html__( 'Enter the text to show in Checkout page', 'yith-woocommerce-points-and-rewards' ),
		'yith-type' => 'textarea-editor',
		'type'      => 'yith-field',
		'default'   => _x( 'If you proceed to checkout, you will earn <strong>{points}</strong> {points_label}!', 'do not translate the text inside the brackets', 'yith-woocommerce-points-and-rewards' ),
		'id'        => 'ywpar_checkout_message',
		'deps'      => array(
			'id'    => 'ywpar_enabled_checkout_message',
			'value' => 'yes',
			'type'  => 'hide',
		),
		'textarea_rows' => 5
	),


	'points_in_cart_checkout_pages_end'            => array(
		'type' => 'sectionend',
		'id'   => 'ywpar_points_in_cart_checkout_pages',
	),

);

return array( 'general' => $section1 );
