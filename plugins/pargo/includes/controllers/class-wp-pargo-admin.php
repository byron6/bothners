<?php


class Wp_Pargo_Admin
{

	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$current_screen = get_current_screen();
		if (strpos($current_screen->base, 'pargo-shipping') === false) {
			echo "";
		} else {
			wp_enqueue_style( 'pargo-bootstrap-style', PARGOPLUGINURL . 'assets/bootstrap/css/bootstrap.css', array(), '3.4.1', 'all');
		}
		if (strpos($current_screen->base, 'pargo-help') === false) {
//			return;
			echo "";
		} else {
			wp_enqueue_style( 'pargo-bootstrap-style', PARGOPLUGINURL . 'assets/bootstrap/css/bootstrap.css', array(), '3.4.1', 'all');
		}

//		wp_enqueue_style( 'pargo-bootstrap-style', PARGOPLUGINURL . 'assets/bootstrap/css/bootstrap.css', array(), '3.4.1', 'all');
//		wp_enqueue_style( 'pargo-main-style', PARGOPLUGINURL . 'assets/css/main.css', array(), $this->version, 'all' );


	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
//		$current_screen = get_current_screen();
//		if (strpos($current_screen->base, 'pargo-shipping-optin') === false) {
//			return;
//		} else {
//			wp_enqueue_script('pargo_tracking_optin_js', plugins_url('assets/js/pargo-tracking-optin.js', PARGOPATH));
//		}

//		wp_enqueue_script( $this->plugin_name, PARGOPATH . 'assets/js/main.js', array( 'jquery' ), $this->version, false );

        wp_enqueue_script('pargo_main_script', PARGOPATH . 'assets/js/main.js', array('jquery'), $this->version, false);
//		wp_enqueue_script( 'pargo-bootstrap-script', PARGOPLUGINURL . 'assets/bootstrap/js/bootstrap.js', array( 'jquery' ), $this->version, false );
//		wp_localize_script('pargo_main_script', 'ajax_object', array('ajaxurl' => plugins_url('ajax-pick-up-point.php', PARGOPLUGINURL)));
        wp_localize_script('pargo_main_script', 'ajax_object', array('ajaxurl' => PARGOPATH . 'ajax-pick-up-point.php'));

	}

}




