/** PARGO 2.4.0 Wordpress Plugin Javascript File **/

jQuery(document).ready(function () {
    localStorage.removeItem("pargo-select");
    let shippingMethod = jQuery("input:radio.shipping_method:checked").val();
    if (!shippingMethod) {
        shippingMethod = jQuery(".shipping_method").val();
    }

    jQuery(document.body).on("updated_checkout", function () {
        jQuery(".shipping_method").change(function () {
            if (jQuery(this).val() !== "wp_pargo") {
                localStorage.removeItem("pargo-select");
            }
        });

        let shippingMethod = jQuery("input:radio.shipping_method:checked").val();
        if (!shippingMethod) {
            shippingMethod = jQuery(".shipping_method").val();
        }
        if (shippingMethod === "wp_pargo") {
            jQuery("#ship-to-different-address").hide();
            jQuery("#parcel_test_field").show();
            jQuery(".insur-info").show();
        } else {
            jQuery("#ship-to-different-address").show();
            jQuery("#parcel_test_field").hide();
            jQuery(".insur-info").hide();
            if (jQuery("#parcel_test").prop("checked")) {
                jQuery("#parcel_test").prop("checked", false);
                jQuery(document.body).trigger("update_checkout");
            }
        }

        if (
            shippingMethod === "wp_pargo" &&
            localStorage.getItem("pargo-select") !== null
        ) {
            jQuery(".pargo-info-box").show();
        } else if (
            shippingMethod === "wp_pargo" &&
            localStorage.getItem("pargo-select") === null
        ) {
            jQuery(".pargo-info-box").hide();
        } else {
            jQuery(".pargo-info-box").hide();
        }
    });

    if (window.addEventListener) {
        window.addEventListener("message", selectPargoPoint, false);
    } else {
        window.attachEvent("onmessage", selectPargoPoint);
    }

    function selectPargoPoint(item) {
        if (item.data.storeName !== undefined) {
            jQuery("#pargo-store-name").text(item.data.storeName);
            jQuery("#pargo-store-address").text(
                item.data.address1 + ", " + item.data.suburb
            );
            jQuery("#pargo-store-hours").text(item.data.businessHours);
            jQuery("#pargo-store-img").prop("src", item.data.photo);
            jQuery(".pargo-info-box").show("slow");

            localStorage.setItem("pargo-select", true);

            jQuery("body").trigger("update_checkout", {
                update_shipping_method: true
            });
            placeOrderBtn(false);
        }
    }

    jQuery(".opening-hours-pargo-btn").click(function () {
        jQuery(".open-hours-pargo-info").toggle("slow");
    });

    jQuery(".pargo-toggle-map").click(function () {
        jQuery(".pargo-info-box").hide("slow");
        jQuery("#pargo_map_block").show("slow");
        jQuery("body").trigger("update_checkout", {update_shipping_method: true});
        placeOrderBtn(false);
        localStorage.removeItem("pargo-select");
    });

    function placeOrderBtn(value) {
        jQuery("#place_order").removeAttr("disabled");
    }
});

jQuery(document).ready(function ($) {
    function openPargoModal() {
        //get value of map token hidden input

        let pargoMapToken = jQuery("#pargomerchantusermaptoken").val();

        jQuery(".pargo-cart").append(
            jQuery(
                "<div id='pargo_map'><div id='pargo_map_center'><div id='pargo_map_inner'><div class='pargo_map_close'>x</div><iframe id='thePargoPageFrameID' src='https://map.pargo.co.za/?token=" +
                pargoMapToken +
                "' width='100%' height='100%' allow='geolocation'  name='thePargoPageFrame' ></iframe></div></div></div>"
            )
        );

        jQuery("#pargo_map").fadeIn(300);

        jQuery(".pargo_map_close").on("click", function () {
            jQuery("#pargo_map").fadeOut(300);
            jQuery('.pargo-cart').empty();
        });
    }

    let pargo_button = "#select_pargo_location_button";

    let body = jQuery("body");

    body.on("click", "#select_pargo_location_button", function () {
        openPargoModal();
    });

    jQuery("#parcel_test_field").after(
        '<span class="insur-info">insurance terms</span>'
    );

    body.on("click", ".insur-info", function () {
        if (jQuery("#parcel_test").is(":checked")) {
            jQuery("#modal-trigger-insurance").attr("checked", "checked");
        } else {
            jQuery("#modal-trigger-insurance").attr("checked", "");
        }
    });

    if (window.addEventListener) {
        window.addEventListener("message", selectPargoPoint, false);
    } else {
        window.attachEvent("onmessage", selectPargoPoint);
    }

    /** After Pargo Pickup Point is Selected **/
    function selectPargoPoint(item) {
        console.log(item);
        let data = null;
        if (item.data.pargoPointCode) {
            localStorage.setItem(
                "pargo-shipping-settings",
                JSON.stringify(item.data)
            );
            data = JSON.parse(localStorage.getItem("pargo-shipping-settings"));
            let pargoButtonCaptionAfter = jQuery("#pargobuttoncaptionafter").val();
            jQuery("#select_pargo_location_button").html(pargoButtonCaptionAfter);
        } else {
            data = JSON.parse(localStorage.getItem("pargo-shipping-settings"));
        }
        let saveData = $.ajax({
            type: "POST",
            url: ajax_object.ajaxurl,
            data: {pargoshipping: data},
            success: function (resultData) {
                if (item.data.photo != "") {
                    jQuery("#pick-up-point-img").attr("src", item.data.photo);
                    jQuery("#pargoStoreName").text(item.data.storeName);
                    jQuery("#pargoStoreAddress").text(item.data.address1);
                    jQuery("#pargoBusinessHours").text(item.data.businessHours);
                }
                //close the map
                if (item.data.pargoPointCode) {
                    jQuery("#pargo_map").hide("slow", function () {
                    });
                    jQuery('.pargo-cart').empty();

                }
            }
        });

    }


    // Click checkout - make sure point is selected

    body.on(
        "click",
        "a.checkout-button.button.alt.wc-forward, #place_order",
        function (e) {
            if (
                jQuery('input[value="wp_pargo"]').is(":checked") ||
                jQuery('input[value="wp_pargo"]').is(":hidden")
            ) {
                if (jQuery("#pargoStoreName").is(":empty")) {
                    e.preventDefault();

                    jQuery("#pargo-not-selected").addClass("show-warn");
                }
            }
        }
    );

    jQuery("#pargo-not-selected").on("click", function () {
        jQuery(this).removeClass("show-warn");
    });

    //end click checkout
});
