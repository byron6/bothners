<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
// Create a helper function for easy SDK access.
function dgoraAsfwFs()
{
    global  $dgoraAsfwFs ;
    
    if ( !isset( $dgoraAsfwFs ) ) {
        // Include Freemius SDK.
        require_once dirname( __FILE__ ) . '/lib/start.php';
        $dgoraAsfwFs = fs_dynamic_init( array(
            'id'             => '700',
            'slug'           => 'ajax-search-for-woocommerce',
            'type'           => 'plugin',
            'public_key'     => 'pk_f4f2a51dbe0aee43de0692db77a3e',
            'is_premium'     => true,
            'premium_suffix' => 'Pro',
            'has_addons'     => false,
            'has_paid_plans' => true,
            'menu'           => array(
            'slug'    => 'dgwt_wcas_settings',
            'support' => false,
        ),
            'is_live'        => true,
        ) );
    }
    
    return $dgoraAsfwFs;
}

// Init Freemius.
dgoraAsfwFs();
// Signal that SDK was initiated.
do_action( 'dgoraAsfwFs_loaded' );
dgoraAsfwFs()->add_filter( 'plugin_icon', function () {
    return dirname( dirname( __FILE__ ) ) . '/assets/img/logo-128.png';
} );
// Uninstall
if ( dgoraAsfwFs()->is__premium_only() ) {
    dgoraAsfwFs()->add_action( 'after_uninstall', function () {
        global  $wpdb ;
        $sitesIds = array();
        $postTypes = get_post_types( array(
            'public'              => true,
            'exclude_from_search' => false,
        ) );
        // DB tables
        $tables = array(
            $wpdb->prefix . 'dgwt_wcas_index',
            $wpdb->prefix . 'dgwt_wcas_tax_index',
            $wpdb->prefix . 'dgwt_wcas_var_index',
            $wpdb->prefix . 'dgwt_wcas_invindex_doclist',
            $wpdb->prefix . 'dgwt_wcas_invindex_wordlist',
            $wpdb->prefix . 'dgwt_wcas_invindex_info',
            $wpdb->prefix . 'dgwt_wcas_invindex_fields',
            $wpdb->prefix . 'dgwt_wcas_invindex_hitlist'
        );
        // Custom posts
        if ( !empty($postTypes) && is_array( $postTypes ) ) {
            foreach ( $postTypes as $postType ) {
                $key = sanitize_key( $postType );
                $tables[] = $wpdb->prefix . 'dgwt_wcas_invindex_doclist_' . $key;
                $tables[] = $wpdb->prefix . 'dgwt_wcas_invindex_wordlist_' . $key;
            }
        }
        
        if ( is_multisite() ) {
            $msTables = array();
            foreach ( get_sites() as $site ) {
                
                if ( is_numeric( $site->blog_id ) && $site->blog_id > 1 ) {
                    $sitesIds[] = $site->blog_id;
                    foreach ( $tables as $table ) {
                        $msTables[] = str_replace( $wpdb->prefix, $wpdb->prefix . $site->blog_id . '_', $table );
                    }
                }
            
            }
            $tables = array_merge( $tables, $msTables );
        }
        
        // Multilingual support
        
        if ( class_exists( 'SitePress' ) || class_exists( 'Polylang' ) ) {
            $langs = array();
            if ( class_exists( 'SitePress' ) ) {
                foreach ( apply_filters( 'wpml_active_languages', null ) as $langCode => $details ) {
                    $langs[] = $langCode;
                }
            }
            if ( class_exists( 'Polylang' ) ) {
                $langs = pll_languages_list( array(
                    'hide_empty' => false,
                    'fields'     => 'slug',
                ) );
            }
            
            if ( is_array( $langs ) ) {
                $langsTables = array();
                foreach ( $langs as $code ) {
                    if ( preg_match( '/^[A-Za-z]{2}$/', $code ) ) {
                        foreach ( $tables as $table ) {
                            if ( preg_match( '/doclist|wordlist/', $table ) ) {
                                $langsTables[] = $table . '_' . $code;
                            }
                        }
                    }
                }
                $langsTables = array_unique( $langsTables );
                $tables = array_merge( $tables, $langsTables );
            }
        
        }
        
        foreach ( $tables as $table ) {
            $wpdb->query( "DROP TABLE IF EXISTS {$table}" );
        }
        // Indexer settings
        delete_option( 'dgwt_wcas_indexer_last_build' );
        delete_transient( 'dgwt_wcas_indexer_details_display' );
        if ( is_multisite() ) {
            foreach ( get_sites() as $site ) {
                
                if ( is_numeric( $site->blog_id ) && $site->blog_id > 1 ) {
                    $table = $wpdb->prefix . $site->blog_id . '_' . 'options';
                    $wpdb->delete( $table, array(
                        'option_name' => 'dgwt_wcas_indexer_last_build',
                    ) );
                    $wpdb->delete( $table, array(
                        'option_name' => '_transient_timeout_dgwt_wcas_indexer_details_display',
                    ) );
                    $wpdb->delete( $table, array(
                        'option_name' => '_transient_dgwt_wcas_indexer_details_display',
                    ) );
                }
            
            }
        }
        // Files
        $upload_dir = wp_upload_dir();
        
        if ( !empty($upload_dir['basedir']) ) {
            $path = $upload_dir['basedir'] . '/wcas-search/';
            
            if ( file_exists( $path ) ) {
                $index = $path . 'products.index';
                if ( file_exists( $index ) ) {
                    unlink( $index );
                }
                rmdir( $path );
            }
        
        }
    
    } );
}