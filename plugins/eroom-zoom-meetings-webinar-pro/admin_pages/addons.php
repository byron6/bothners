<?php
if( class_exists( 'StmZoom' ) ) {
    $addons = StmZoomPro::addons();
    $enabled_addons = get_option( 'stm_zoom_addons', array() );
    if( !empty( $addons ) ): ?>
        <div class="stm_zoom_addons">
            <h1><?php esc_html_e('eRoom Pro Addons', 'eroom-zoom-meetings-webinar-pro'); ?></h1>
            <?php foreach( $addons as $addon ): ?>
                <?php
                $slug = $addon[ 'slug' ];
                $status = 'disable';
                if( !empty( $enabled_addons ) && !empty( $enabled_addons[ $slug ] ) ) {
                    $status = $enabled_addons[ $slug ];
                }
                ?>
                <div class="addon_item <?php echo esc_attr($status); ?>">
                    <div class="addon-wrap">
                        <div class="addon_name">
                            <?php echo esc_html( $addon[ 'name' ] ); ?>
                        </div>
                        <div class="addon_action addon_enable">
                            <a href="#" data-status="enable"
                               data-addon="<?php echo esc_attr( $slug ); ?>"><?php esc_html_e( 'Enable', 'eroom-zoom-meetings-webinar-pro' ); ?></a>
                        </div>
                        <div class="addon_action addon_disable">
                            <a href="#" data-status="disable"
                               data-addon="<?php echo esc_attr( $slug ); ?>"><?php esc_html_e( 'Disable', 'eroom-zoom-meetings-webinar-pro' ); ?></a>
                        </div>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    <?php
    endif;
}