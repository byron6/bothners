jQuery( document ).ready(function() {
	//Kerridge API Call
	jQuery("select[name='api-methods']").on('change', function() {
		console.log(jQuery("select[name='api-methods']").val());
		jQuery.ajax({
			url:asc_ajax_object.ajax_url,
			data:{ 
			  action: 'kerridge_api',
			  api_method: jQuery("select[name='api-methods']").val(),
			  api_acc_num: jQuery("input[name='api-account-num']").val()
			},
			success:function(data){
				jQuery('#api-results').html(data); // insert data
			}
		});
	});
});