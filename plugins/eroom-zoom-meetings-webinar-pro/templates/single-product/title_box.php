<?php
$post_id        = get_the_ID();
$meeting_id     = get_post_meta( $post_id, '_meeting_id', true );
$meeting_data   = StmZoom::meeting_time_data( $meeting_id );
$is_started     = false;
$meeting_over   = get_post_meta( $post_id, '_meeting_over', true );
$is_purchased   = stm_zoom_has_bought_items( array( $post_id ) );
if ( ! empty( $meeting_data ) && isset( $meeting_data['is_started'] ) ) {
    $classes    = $meeting_data[ 'is_started' ] ? 'meeting_started ' : 'meeting_not_started ';
    $classes    .= $is_purchased ? 'purchased ' : 'not_purchased ';
    $is_started = $meeting_data[ 'is_started' ];
}
?>
<div class="stm_zoom_product_title_box <?php echo esc_attr( $classes ); ?>">
    <div class="meeting-title-info">
        <h1 class="meeting_title"><?php the_title(); ?></h1>
        <?php if ( ! empty( $meeting_id ) ) : ?>
            <?php
            $date       = get_post_meta( $meeting_id, 'stm_date', true );
            $time       = get_post_meta( $meeting_id, 'stm_time', true );
            $timezone   = get_post_meta( $meeting_id, 'stm_timezone', true );
            if ( ! empty( $date ) ) {
                $date   = strtotime( 'today', ( intval( $date ) / 1000 ) );
                ?>
                <div class="meeting_date">
                    <?php echo date( 'd M Y', $date ); ?>
                    <?php if ( ! empty( $time ) ) echo ' - ' . esc_html( $time ); ?>
                </div>
                <?php if ( ! empty( $timezone ) ) : ?>
                    <div class="meeting_timezone">
                        <?php
                        if ( function_exists( 'stm_zoom_get_timezone_options') ) {
                            $timezones = stm_zoom_get_timezone_options();
                            if ( ! empty( $timezones[ $timezone ] ) ) {
                                $timezone = $timezones[$timezone];
                            }
                        }
                        ?>
                        <?php echo esc_html( $timezone ); ?>
                    </div>
                <?php endif; ?>
            <?php } ?>
        <?php endif; ?>
    </div>

    <?php if ( has_post_thumbnail() ) : ?>
        <div class="meeting-image">
            <?php the_post_thumbnail(); ?>
        </div>
    <?php endif; ?>
</div>
<?php

if ( $is_started && $is_purchased && ! $meeting_over ) {
    echo do_shortcode('[stm_zoom_conference post_id="'. $meeting_id . '"]');
}
