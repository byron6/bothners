<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Kerridge Test Form
function api_test_platform(){
	$html = '
	<div id="api-requests" class="col-sml-12 col-md-6">
		<p>Working Account Number: <b>ABI001</b></p>
		<div class="sem-filters">
			<select name="api-methods">
				<option value="">Select an action...</option>
				<option value="GetAccountDetails">Get Account Details</option>
				<option value="GetAccountHistory">Get Account History</option>
				<option value="GetBranches">Branch List</option>
				<option value="GetInvoices">Get Invoices</option>
			</select><br>
			Acc Num: <input name="api-account-num" type="text" value="ABI001" />
		</div>
	</div>
	<div id="api-results" class="col-sml-12 col-md-6">
	
	</div>
	';
	
	return $html;
}
add_shortcode('api-test-platform', 'api_test_platform');
?>