*** WooCommerce Contact for Shipping Quote Changelog ***

2020.04.01 - version 1.1.1
* [Fix] - Meta data recovery from email notification not always working
* [Add] - Hook 'WCCSQ/get_cart_data'

2019.11.26 - version 1.1.0
* [Add] - Email notifications for store owner and customers
* [Add] - Required data setting
* [Add] - Add customer email to shipping quote data
* [Add] - 'show-if-available', 'hide-if-available' CSS classes
* [Add] - 'hide_when_available' attribute for request shortcode
* [Fix] - Shipping class requirement for variations with 'Same as parent'

2019.08.08 - version 1.0.1
* [Fix] - Default WC shipping settings showing on settings page on WC 3.6
* [Fix] - 'Enable when cart contains' shipping instance setting not saving
* [Fix] - Quotes not always pushed to the admin/front-end due to cache
* [Add] - Additional filters/hooks

2019.06.11 - version 1.0.0
* Initial release.
